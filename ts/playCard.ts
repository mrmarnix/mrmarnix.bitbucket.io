class PlayCard {

    private _message : Message;
    private _useful : Useful;
    private _cardHandeling : CardHandeling;

    private _tempValue : any; /* :any as it uses a string and converts itself to a number in the function playcard. */
    public _decklistP2 : Array<number>;
    private _prizelistP2 : Array<number>;
    public _prizelistP1 : Array<number>;
    private _wonP2 : number;
    public _wonP1 : number;
    public _cardSlot1 : boolean;
    public _cardSlot2 : boolean;
    public _cardSlot3 : boolean;

    constructor() {
        this._tempValue = 0;
        this._decklistP2 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        this._prizelistP2 = [];
        this._prizelistP1 = [];
        this._wonP2 = 0;
        this._wonP1 = 0;
        this._cardSlot1 = true;
        this._cardSlot2 = true;
        this._cardSlot3 = true;
    }

    public playInventoryCard(cardNumber:number) : void { //Play a card from your inventory.
        if (document.getElementById("inventoryCard" + cardNumber + "P1").innerHTML != "-") {
            switch (cardNumber) {
                case 1:
                    this._cardSlot1 = false;
                break;
                case 2:
                    this._cardSlot2 = false;
                break;
                case 3:
                    this._cardSlot3 = false;
                break;
            }
            this.playCard("inventoryCard" + cardNumber + "P1");
            this._useful.replace("textBox", this._message.getBattle);
        }

    }

    private playCard(tempId:string) : void { /* Hides the played card and stores the value of the played card. */
        this._tempValue = document.getElementById(tempId).innerHTML;
        this._useful.hide(tempId);
        this._tempValue = parseInt(this._tempValue);
        let cardId = Math.floor(Math.random() * 3) + 1;
        this._useful.hide("inventoryCard" + cardId + "P2");
        app.deckEmptyP2();
        this.cardIsPlayed(this._tempValue);
    }

    private cardIsPlayed(value:number) : void { /* The played card enters the middle area. */
        let p2Value = app._decklistP2.splice(Math.floor(Math.random()*app._decklistP2.length), 1);
        this._useful.unhide("playedCardP1");
        this._useful.unhide("playedCardP2");
        this._useful.replace("playedCardP1", value);
        this._useful.replace("playedCardP2", p2Value);
        setTimeout(this.checkForHigher, 1000, value, p2Value); //Number here indicates time before cards are checked and removed.
    }

    private checkForHigher = (value:number, p2Value:number) : void => { /* Checks which card is higher and distibutes a point to the winner. */
        this._useful.hide("playedCardP1");
        this._useful.hide("playedCardP2");
        if (value > p2Value) { /* Player 1 won */
            app._wonP1 += 2;
            this._useful.replace("prizeCardP1", app._wonP1);
            this._useful.replace("textBox", this._message.getWin);
            console.log(this._message.getWin);
            app.addPrizeCard(1, value, p2Value);
        } else if (p2Value > value) {
            app._wonP2 += 2;
            this._useful.replace("prizeCardP2", app._wonP2);
            this._useful.replace("textBox", this._message.getLoss);
            console.log(this._message.getLoss);
            app.addPrizeCard(2, value, p2Value);
        } else {
            this._useful.replace("textBox", this._message.getDraw);
            console.log(this._message.getDraw);
        }
        if (app._wonP1 > 0) {
            this._useful.unhide("prizeCardP1");
        }
        if (app._wonP2 > 0) {
            this._useful.unhide("prizeCardP2");
        }
        this.fillCard();
    }

    private deckEmptyP2() : void { /* If player 2 has no remaining cards, his prizepool will be his new deck. */
        if (this._decklistP2.length === 0) {
            console.log("Player 2 reshuffles..");
            this._decklistP2 = this._prizelistP2;
            this._prizelistP2 = [];
            this._useful.replace("deckAmountP2", this._decklistP2.length-3);
            this._wonP2 = 0;
            this._useful.hide("prizeCardP2");
        } else {
            
        }
    }

    private fillCard() : void { /* Fils up the empty spot from the played card. */
        let slots = [, app._cardSlot1, app._cardSlot2, app._cardSlot3];
        for (let i = 1; i < slots.length; i++) {
            if (slots[i] === false) {
                this._useful.unhide("inventoryCard" + i + "P1");
                this._useful.replace("inventoryCard" + i + "P1", this._cardHandeling.getCard());
            }
        }
        this._cardSlot1 = true;
        this._cardSlot2 = true;
        this._cardSlot3 = true;
        for (let i = 1; i < 4; i++) {    
        this._useful.unhide("inventoryCard" + i + "P2");
        }
    }

}