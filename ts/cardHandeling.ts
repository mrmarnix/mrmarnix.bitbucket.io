class CardHandeling {
    
    // Commands used after the two cards got compared.
    private _useful : Useful;
    
    private addPrizeCard(winnerPlayer:number, prizeP1:number, prizeP2:number) : void { /* Add both cards won by target player. */
        if (winnerPlayer === 1) {
            app._prizelistP1.push(prizeP1, prizeP2);
        }
        if (winnerPlayer === 2) {
            app._prizelistP2.push(prizeP1, prizeP2);
        } 
    }

    public getCard() : any { /* Gives a random card to P1, if it can't it will display a "-" (Therefor the :any) */
        if (app._decklistP1.length > 0) {
            let returnNumber = app._decklistP1.splice(Math.floor(Math.random()*app._decklistP1.length), 1);
            this.decreaseDeck();
            return returnNumber;
        } else {
            return "-";
        }
    }

    private decreaseDeck() : void { /* When you draw a card your deck decreases with 1.  */
        app._tempValue = document.getElementById("deckAmountP1").innerHTML;
        if (app._tempValue > 0) {
            app._tempValue--;
            this._useful.replace("deckAmountP1", app._tempValue);
            // replace("deckAmountP2", tempValue);
        }
        app._tempValue2 = document.getElementById("deckAmountP2").innerHTML;
        if (app._tempValue2 > 0) {
            app._tempValue2--;
            this._useful.replace("deckAmountP2", app._tempValue2);
        }
    }

}