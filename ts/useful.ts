class Useful {
        
        // Basic functions, used to help other functions. All functions are public as they are used in other classes.
        public hide(id:string) : void { /* Hides target element, used for hiding cards. */
            document.getElementById(id).style.visibility = "hidden";   
        }
    
        public unhide(id:string) : void { /* Unhides target element, used for unhiding cards. */
            document.getElementById(id).style.visibility = "visible";   
        }
    
        public replace(id:string, replacement:any) : void { /* Replace the text or number (therefor :any) of target element with somthing else.*/
            document.getElementById(id).innerHTML = replacement;
        }
}