class Game {
    private _message : Message;
    private _playCard : PlayCard;
    private _useful : Useful;
    private _cardHandeling : CardHandeling;

    
    private _decklistP1 : Array<number>;

    constructor(){
        this._message = new Message;
        this._playCard = new PlayCard;
        this._useful = new Useful;
        
        this._decklistP1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];  
        this.startState();
        window.addEventListener('keydown', this.keyDownHandler);
        document.getElementById("restartButton").addEventListener('click', this.restart);
    }


    // Startingsstate of the game.
    private startState() : void {
        this._useful.hide("prizeCardP2");
        this._useful.hide("prizeCardP1");
        this._useful.hide("playedCardP2");
        this._useful.hide("playedCardP1");
        this._useful.replace("deckAmountP2", this._playCard._decklistP2.length);
        this._useful.replace("deckAmountP1", this._playCard._decklistP2.length);
        this._useful.replace("inventoryCard1P1", this._cardHandeling.getCard());
        this._useful.replace("inventoryCard2P1", this._cardHandeling.getCard());
        this._useful.replace("inventoryCard3P1", this._cardHandeling.getCard());
    }

    private keyDownHandler = (e:KeyboardEvent) : void => { // Play a card with target keycode (1,2,3)
        if (e.keyCode === 49) { /* Plays first card. */
            console.log("You played card 1.");
            this._playCard.playInventoryCard(1);
        } else if (e.keyCode === 50) { /* Plays second card. */
            console.log("You played card 2.");
            this._playCard.playInventoryCard(2);
        } else if (e.keyCode === 51) { //* Plays third card. */
            console.log("You played card 3.");
            this._playCard.playInventoryCard(3);
        }
    }

    // Commands for when your deck pile is up.
    private restart = () : void => { /* Checks if transferPrizeToDeck needs to be called. */
        if (document.getElementById("inventoryCard1P1").innerHTML === "-") {
            if (document.getElementById("inventoryCard2P1").innerHTML === "-") {
                if (document.getElementById("inventoryCard3P1").innerHTML === "-") {
                    console.log("Reshuffling..");
                    console.log(this);
                    this._decklistP1 = this._playCard._prizelistP1;
                    this._playCard._prizelistP1 = [];
                    this._useful.replace("deckAmountP1", this._decklistP1.length);
                    this._useful.replace("inventoryCard1P1", this._cardHandeling.getCard());
                    this._useful.replace("inventoryCard2P1", this._cardHandeling.getCard());
                    this._useful.replace("inventoryCard3P1", this._cardHandeling.getCard());
                    this._playCard._wonP1 = 0;
                    this._useful.hide("prizeCardP1");           
                }
            }
        }
    }
}
