class Message {
    
    // Functions to display messages in the infobox. All functions are public as they are used in other classes.
    private _win : string;
    private _loss : string;
    private _draw : string;
    private _battle : string;

    constructor() {
        this._win = "You won!";
        this._loss = "You lost.";
        this._draw = "Draw!";
        this._battle = "Waiting..";
    }

    public getWin() : String {
        return this._win;
    }

    public getLoss() : String {
        return this._loss;
    }

    public getDraw() : String {
        return this._draw;
    }

    public getBattle() : String {
        return this._battle;
    }

}