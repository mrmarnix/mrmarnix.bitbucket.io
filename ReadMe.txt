Documentatie:
Over het spel:
Ik heb het spel eerst in Javascript gemaakt omdat Typescript mij niet lukte. Zelf heb ik rond de 2 � 3 dagen besteed aan
tutorials en filmpjes om te kijken of ik het daarna wel kon snappen, maar dat lukte niet. Daarom heb ik dus gekozen
voor javascript om zo toch nog een resultaat neer te zetten. Nu heb ik voor mijn herkansing toch nog wat meer verdiept in Typescript.
Met kleine stapjes heb ik uiteindelijk steeds meer kunnen doen met Typescript. Ik heb nu mijn hele spel omgezet in Typscript.
Er zitten nog wat technische foutjes in de code (zoals :any gebruiken en public functions) maar verder ben ik redelijk tevreden over mijn resultaat.

Hoe speel je het spel:
Je begint het spel met 3 kaarten die rechtsonder in je inventory zitten. Je kan op je toetsenbord op 1, 2 of 3 klikken om te spelen.
Vervolgens word er gekeken welke gespeelde kaart hoger is (die van jou of die van je tegenstander). De speler die de hogere
kaart had gespeeld krijgt beide kaarten in zijn prijzenpot. Als je deck op is en je inventory geen kaarten bevat kan je op
"Reshuffle" (knop rechts) klikken om de gewonnen prijzen als je nieuwe deck te spelen. Voor meer informatie kan je over de
informatieknop (witte knop links) vegen met je muis om een tooltip met wat informatie tevoorschijn te halen.