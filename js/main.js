var Card = (function () {
    function Card(value) {
        this._value = value;
    }
    return Card;
}());
var Game = (function () {
    function Game() {
        var _this = this;
        this.keyDownHandler = function (e) {
            if (e.keyCode === 49) {
                console.log("You played card 1.");
                _this.playInventoryCard1();
            }
            else if (e.keyCode === 50) {
                console.log("You played card 2.");
                _this.playInventoryCard2();
            }
            else if (e.keyCode === 51) {
                console.log("You played card 3.");
                _this.playInventoryCard3();
            }
        };
        this.checkForHigher = function (value, p2Value) {
            _this.hide("playedCardP1");
            _this.hide("playedCardP2");
            if (value > p2Value) {
                _this._wonP1 += 2;
                _this.replace("prizeCardP1", _this._wonP1);
                _this.replace("textBox", _this._win);
                console.log(_this._win);
                _this.addPrizeCard(1, value, p2Value);
            }
            else if (p2Value > value) {
                _this._wonP2 += 2;
                _this.replace("prizeCardP2", _this._wonP2);
                _this.replace("textBox", _this._loss);
                console.log(_this._loss);
                _this.addPrizeCard(2, value, p2Value);
            }
            else {
                _this.replace("textBox", _this._draw);
                console.log(_this._draw);
            }
            if (_this._wonP1 > 0) {
                _this.unhide("prizeCardP1");
            }
            if (_this._wonP2 > 0) {
                _this.unhide("prizeCardP2");
            }
            _this.fillCard();
        };
        this.restart = function () {
            if (document.getElementById("inventoryCard1P1").innerHTML === "-") {
                if (document.getElementById("inventoryCard2P1").innerHTML === "-") {
                    if (document.getElementById("inventoryCard3P1").innerHTML === "-") {
                        console.log("Reshuffling..");
                        console.log(_this);
                        _this._decklistP1 = _this._prizelistP1;
                        _this._prizelistP1 = [];
                        _this.replace("deckAmountP1", _this._decklistP1.length);
                        _this.replace("inventoryCard1P1", _this.getCard());
                        _this.replace("inventoryCard2P1", _this.getCard());
                        _this.replace("inventoryCard3P1", _this.getCard());
                        _this._wonP1 = 0;
                        _this.hide("prizeCardP1");
                    }
                }
            }
        };
        this._decklistP2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        this._decklistP1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        this._prizelistP2 = [];
        this._prizelistP1 = [];
        this._tempValue = 0;
        this._wonP2 = 0;
        this._wonP1 = 0;
        this._cardSlot1 = true;
        this._cardSlot2 = true;
        this._cardSlot3 = true;
        this._win = "You won!";
        this._loss = "You lost.";
        this._draw = "Draw!";
        this._battle = "Waiting..";
        this.startState();
        window.addEventListener('keydown', this.keyDownHandler);
        window.addEventListener('click', this.restart);
    }
    Game.prototype.hide = function (id) {
        document.getElementById(id).style.visibility = "hidden";
    };
    Game.prototype.unhide = function (id) {
        document.getElementById(id).style.visibility = "visible";
    };
    Game.prototype.replace = function (id, replacement) {
        document.getElementById(id).innerHTML = replacement;
    };
    Game.prototype.startState = function () {
        this.hide("prizeCardP2");
        this.hide("prizeCardP1");
        this.hide("playedCardP2");
        this.hide("playedCardP1");
        this.replace("deckAmountP2", this._decklistP2.length);
        this.replace("deckAmountP1", this._decklistP2.length);
        this.replace("inventoryCard1P1", this.getCard());
        this.replace("inventoryCard2P1", this.getCard());
        this.replace("inventoryCard3P1", this.getCard());
    };
    Game.prototype.playInventoryCard1 = function () {
        if (document.getElementById("inventoryCard1P1").innerHTML != "-") {
            this._cardSlot1 = false;
            this.playCard("inventoryCard1P1");
            this.replace("textBox", this._battle);
        }
    };
    Game.prototype.playInventoryCard2 = function () {
        if (document.getElementById("inventoryCard2P1").innerHTML != "-") {
            this._cardSlot2 = false;
            this.playCard("inventoryCard2P1");
            this.replace("textBox", this._battle);
        }
    };
    Game.prototype.playInventoryCard3 = function () {
        if (document.getElementById("inventoryCard3P1").innerHTML != "-") {
            this._cardSlot3 = false;
            this.playCard("inventoryCard3P1");
            this.replace("textBox", this._battle);
        }
    };
    Game.prototype.playCard = function (tempId) {
        this._tempValue = document.getElementById(tempId).innerHTML;
        this.hide(tempId);
        this._tempValue = parseInt(this._tempValue);
        var cardId = Math.floor(Math.random() * 3) + 1;
        this.hide("inventoryCard" + cardId + "P2");
        this.deckEmptyP2();
        this.cardIsPlayed(this._tempValue);
    };
    Game.prototype.cardIsPlayed = function (value) {
        var p2Value = this._decklistP2.splice(Math.floor(Math.random() * this._decklistP2.length), 1);
        this.unhide("playedCardP1");
        this.unhide("playedCardP2");
        this.replace("playedCardP1", value);
        this.replace("playedCardP2", p2Value);
        setTimeout(this.checkForHigher, 1000, value, p2Value);
    };
    Game.prototype.addPrizeCard = function (winnerPlayer, prizeP1, prizeP2) {
        if (winnerPlayer === 1) {
            this._prizelistP1.push(prizeP1, prizeP2);
        }
        if (winnerPlayer === 2) {
            this._prizelistP2.push(prizeP1, prizeP2);
        }
    };
    Game.prototype.fillCard = function () {
        var slots = [, this._cardSlot1, this._cardSlot2, this._cardSlot3];
        for (var i = 1; i < slots.length; i++) {
            if (slots[i] === false) {
                this.unhide("inventoryCard" + i + "P1");
                this.replace("inventoryCard" + i + "P1", this.getCard());
            }
        }
        this._cardSlot1 = true;
        this._cardSlot2 = true;
        this._cardSlot3 = true;
        for (var i = 1; i < 4; i++) {
            this.unhide("inventoryCard" + i + "P2");
        }
    };
    Game.prototype.getCard = function () {
        if (this._decklistP1.length > 0) {
            var returnNumber = this._decklistP1.splice(Math.floor(Math.random() * this._decklistP1.length), 1);
            this.decreaseDeck();
            return returnNumber;
        }
        else {
            return "-";
        }
    };
    Game.prototype.decreaseDeck = function () {
        this._tempValue = document.getElementById("deckAmountP1").innerHTML;
        if (this._tempValue > 0) {
            this._tempValue--;
            this.replace("deckAmountP1", this._tempValue);
        }
        this._tempValue2 = document.getElementById("deckAmountP2").innerHTML;
        if (this._tempValue2 > 0) {
            this._tempValue2--;
            this.replace("deckAmountP2", this._tempValue2);
        }
    };
    Game.prototype.deckEmptyP2 = function () {
        if (this._decklistP2.length === 0) {
            console.log("Player 2 reshuffles..");
            this._decklistP2 = this._prizelistP2;
            this._prizelistP2 = [];
            this.replace("deckAmountP2", this._decklistP2.length - 3);
            this._wonP2 = 0;
            this.hide("prizeCardP2");
        }
        else {
        }
    };
    return Game;
}());
var app;
(function () {
    var init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
//# sourceMappingURL=main.js.map